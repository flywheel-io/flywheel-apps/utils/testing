# Decorators

The `fw_gear_testing.decorators` module provides decorators for profiling gear execution.

## `report_open_fds`

This decorator reports on the number of open File Descriptors before and after a function is called.  Mainly this should be used when a suspected socket or open files leak is happening.

Ex.:

```python
from fw_gear_testing.decorators import report_open_fds
import requests

@report_open_fds(True)
def make_heavy_request(num):
    for i in range(1,num):
        requests.get('https://google.com')
make_heavy_request(10)
```
Output:
```
...
DEBUG:urllib3.connectionpool:Starting new HTTPS connection (1): www.google.com:443
DEBUG:urllib3.connectionpool:https://www.google.com:443 "GET / HTTP/1.1" 200 None
DEBUG:fw_gear_testing.decorators:Number of fds (make_heavy_request) before: 0, after: 0
```



