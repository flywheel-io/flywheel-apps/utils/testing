# SDK

This module contains various mocks and fixtures relating to the flywheel SDK.

## `sdk_mock`

This fixture when included in a function mocks the flywheel SDK.  It patches the `flywheel.Client` function and returns a MagicMock containing all the functions in the SDK.

You can set the return value for anything in the SDK through this interface:

```python
def test_get_acquisition(sdk_mock):
    sdk_mock.get_acquisition.return_value = flywheel.Acquisition(
        {'label':'test'}
    )
    main.run()
    sdk_mock.get_acquisition.assert_called_once()
```

## `get_job`

This fixture mocks the result of getting a job.  You can pass in (optionall) a job id, inputs dictionary, config dictionary, and gear_info.