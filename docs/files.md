# Files

These utilies require the optional dependencies `pydicom` and `fw-file`

## `create_dcm`

This function will create an `fw_file.dicom.DICOM` with the specifified parameters.

You can pass in a custom file, preamble, file metadata, and dicom tag values.

By default `create_dcm` will create a dicom with the following dataset:

```python
>>> from fw_gear_testing.files import create_dcm
>>> dcm = create_dcm()
>>> dcm
DICOM(None)
>>> dcm.dataset.raw
Dataset.file_meta -------------------------------
(0002, 0000) File Meta Information Group Length  UL: 146
(0002, 0001) File Meta Information Version       OB: b'\x00\x01'
(0002, 0002) Media Storage SOP Class UID         UI: MR Image Storage
(0002, 0003) Media Storage SOP Instance UID      UI: 1.2.3
(0002, 0010) Transfer Syntax UID                 UI: Implicit VR Little Endian
(0002, 0012) Implementation Class UID            UI: 1.2.826.0.1.3680043.8.498.1
(0002, 0013) Implementation Version Name         SH: 'PYDICOM 2.1.2'
-------------------------------------------------
(0008, 0016) SOP Class UID                       UI: MR Image Storage
(0008, 0018) SOP Instance UID                    UI: 1.2.3
(0010, 0020) Patient ID                          LO: 'test'
(0020, 000d) Study Instance UID                  UI: 1
(0020, 000e) Series Instance UID                 UI: 1.2
```

You can also pass in any dictionary with the format `{<dicom_tag>:<value>}` to have that tag added to the dataset.

The `create_dcm` function will try to look up the VR for any tag you pass it, however, you can also explicitely set the VR and value by passing in a dictionary with the format:

```python
{
    <key>: (<VR>, <value>)
}
```

Ex.:
```python
>>> from fw_gear_testing.files import create_dcm
>>> dcm = create_dcm(**{'ImageOrientationPatient':[0,1,2,3,4,5]})
>>> dcm.get('ImageOrientationPatient')
[0.0, 1.0, 2.0, 3.0, 4.0, 5.0]
```

For more details on the DICOM class works, please see [fw-file documentation](https://gitlab.com/flywheel-io/tools/lib/fw-file)