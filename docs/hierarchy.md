# Hierarchy

This module contains various mocks for the flywheel hierarchy.

The main entrypoint in this module is the `fw_project` fixture which accepts arguments for number of subjects (`n_subjects`), sessions (`n_sessions`), acquisitions (`n_acquisitions`), and files (`n_files`).  This fixture when called will return a mocked Flywheel hierarchy which can be used in tests.