# Release Notes

## 0.2.4

__Enhancements__:
* Add gear mocks: make_gear, gear_fixture.
* Add user mock in SDK
* Add gear related files: Manifest, Dockerfile, and run.py in memory mocks.

## 0.2.3

__Enhancements__:

* Add object and file id generation functions and fixtures
* Add `iter_find`, `find` and `find_first` to `MockFinder`
* Refactor `fw_project` to have mock container generators at every level,
Expose each as a method and a fixture:
    * `make_acquisition`, fixture: `fw_acquisition`
    * `make_session`, fixture: `fw_session`
    * `make_subject`, fixture: `fw_subject`
    * `make_project`, fixture: `fw_project`

