# Contributing

## Getting started

First steps after cloning the repo:
1. `poetry install`: Install project and all dependencies (see __Dependency management__ below)
2. `pre-commit install`: Install pre-commit hooks (see __Linting and Testing__ below)

## Repository Structure
The main plugin lives in the `fw_gear_testing/` folder which includes the following:
```
 fw_gear_testing
├── files.py         Fixtures for dealing with files, mostly DICOMs
├── hierarchy.py     Fixtures and mocks for the Flywheel Hierarchy
├── __init__.py
├── sdk.py           Fixtures and mocks for the Flywheel SDK
└── utils.py         Utility Fixtures
```

## Developing fixtures and mocks
Please refer to the [pytest documentation](https://docs.pytest.org/en/stable/fixture.html).

## Dependency management
This gear uses [`poetry`](https://python-poetry.org/) to manage dependencies, 
develop, build and publish.

### Dependencies

Dependencies are listed in the `pyproject.toml` file. 

#### Managing dependencies
* Adding: Use `poetry add [--dev] <dep>`
* Removing: Use `poetry remove [--dev] <dep>`
* Updating: Use `poetry update <dep>` or `poetry update` to update all deps.
  * Can also not udpate development dependencies with `--no-dev`
  * Update dry run: `--dry-run`

#### Using a different version of python
Poetry manages virtual environments and can create a virtual environment with different versions of python, however that version must be installed on the machine.  

You can configure the python version by using `poetry env use <path/to/executable>`

#### Helpful poetry config options
See full options [Here](https://python-poetry.org/docs/configuration/#available-settings).

List current config: `poetry config --list`

* `poetry config virtualenvs.in-project <true|false|None>`: create virtual environment inside project directory
* `poetry config virtualenvs.path <path>`: Path to virtual environment directory.

## Linting and Testing
Local linting and testing scripts are managed through [`pre-commit`](https://pre-commit.com/).  
Pre-commit allows running hooks which can be defined locally, or in other 
repositories. Default hooks to run on each commit:

* check-json: JSON syntax validator
* isort: Run isort in poetry venv
* black: Run black in poetry venv

These hooks will all run automatically on commit, but can also be run manually 
or just be disabled.

### pre-commit usage:

* Run hooks manually:
    * Run on all files: `pre-commit run -a`
    * Run on certain files: `pre-commit run --files test/*`
* Update hooks: `pre-commit autoupdate`
* Disable all hooks: `pre-commit uninstall`
* Enable all hooks: `pre-commit install`
* Skip a hook on commit: `SKIP=<hook-name> git commit`
* Skip all hooks on commit: `git commit --no-verify`
