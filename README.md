# Flywheel Gear Testing Suite

This repository is meant to serve as a collection of fixtures and helpers for testing a gear.  Currently the interface for using these fixtures is as a [pytest plugin](https://docs.pytest.org/en/latest/plugins.html).

## Usage

In your gear repository, you can include these fixtures as a pytest plugin.

1. Install this package using poetry (or your python package manager of choice):
`poetry add --dev fw-gear-testing`
2. Within your testing folder create a `conftest.py` file if you don't already have one.
3. Within the `conftest.py` file add the following lines:

```python
import pytest
pytest_plugins = ('fw_gear_testing',)
```